
// automatically generated by luatablegen
#include "../lua-5.3.4/src/lua.h"
#include "../lua-5.3.4/src/lauxlib.h"
#include "../lua-5.3.4/src/lualib.h"
#include <inttypes.h>
#include <stdbool.h>

#ifndef _W_Start_Section_H
#define _W_Start_Section_H
#ifdef __cplusplus
extern "C" {
#endif

#include "../wasm.h"

static W_Start_Section* convert_W_Start_Section (lua_State* __ls, int index);
static W_Start_Section* check_W_Start_Section(lua_State* __ls, int index);
W_Start_Section* push_W_Start_Section(lua_State* __ls);
int W_Start_Section_push_args(lua_State* __ls, W_Start_Section* _st);
int new_W_Start_Section(lua_State* __ls);
static int getter_W_Start_Section_index(lua_State* __ls);
static int setter_W_Start_Section_index(lua_State* __ls);
int W_Start_Section_register(lua_State* __ls);

#ifdef __cplusplus
}
#endif //end of extern c
#endif //end of inclusion guard


