
// automatically generated by luatablegen
#include "../lua-5.3.4/src/lua.h"
#include "../lua-5.3.4/src/lauxlib.h"
#include "../lua-5.3.4/src/lualib.h"
#include <inttypes.h>
#include <stdbool.h>

#ifndef _W_Import_Section_Entry_H
#define _W_Import_Section_Entry_H
#ifdef __cplusplus
extern "C" {
#endif

#include "../wasm.h"

static W_Import_Section_Entry* convert_W_Import_Section_Entry (lua_State* __ls, int index);
static W_Import_Section_Entry* check_W_Import_Section_Entry(lua_State* __ls, int index);
W_Import_Section_Entry* push_W_Import_Section_Entry(lua_State* __ls);
int W_Import_Section_Entry_push_args(lua_State* __ls, W_Import_Section_Entry* _st);
int new_W_Import_Section_Entry(lua_State* __ls);
static int getter_W_Import_Section_Entry_module_length(lua_State* __ls);
static int getter_W_Import_Section_Entry_module_str(lua_State* __ls);
static int getter_W_Import_Section_Entry_field_len(lua_State* __ls);
static int getter_W_Import_Section_Entry_field_str(lua_State* __ls);
static int getter_W_Import_Section_Entry_kind(lua_State* __ls);
static int getter_W_Import_Section_Entry_type(lua_State* __ls);
static int setter_W_Import_Section_Entry_module_length(lua_State* __ls);
static int setter_W_Import_Section_Entry_module_str(lua_State* __ls);
static int setter_W_Import_Section_Entry_field_len(lua_State* __ls);
static int setter_W_Import_Section_Entry_field_str(lua_State* __ls);
static int setter_W_Import_Section_Entry_kind(lua_State* __ls);
static int setter_W_Import_Section_Entry_type(lua_State* __ls);
int W_Import_Section_Entry_register(lua_State* __ls);

#ifdef __cplusplus
}
#endif //end of extern c
#endif //end of inclusion guard


