
// automatically generated by luatablegen
#include "../lua-5.3.4/src/lua.h"
#include "../lua-5.3.4/src/lauxlib.h"
#include "../lua-5.3.4/src/lualib.h"
#include <inttypes.h>
#include <stdbool.h>
#include "./W_Elem_Segment_tablegen.h"

#include "../wasm.h"

static W_Elem_Segment* convert_W_Elem_Segment (lua_State* __ls, int index) {
	W_Elem_Segment* dummy = (W_Elem_Segment*)lua_touserdata(__ls, index);
	if (dummy == NULL) printf("W_Elem_Segment:bad user data type.\n");
	return dummy;
}

static W_Elem_Segment* check_W_Elem_Segment(lua_State* __ls, int index) {
	W_Elem_Segment* dummy;
	luaL_checktype(__ls, index, LUA_TUSERDATA);
	dummy = (W_Elem_Segment*)luaL_checkudata(__ls, index, "W_Elem_Segment");
	if (dummy == NULL) printf("W_Elem_Segment:bad user data type.\n");
	return dummy;
}

W_Elem_Segment* push_W_Elem_Segment(lua_State* __ls) {
	lua_checkstack(__ls, 1);
	W_Elem_Segment* dummy = lua_newuserdata(__ls, sizeof(W_Elem_Segment));
	luaL_getmetatable(__ls, "W_Elem_Segment");
	lua_setmetatable(__ls, -2);
	return dummy;
}

int W_Elem_Segment_push_args(lua_State* __ls, W_Elem_Segment* _st) {
	lua_checkstack(__ls, 4);
	lua_pushinteger(__ls, _st->index);
	lua_pushlightuserdata(__ls, _st->offset);
	lua_pushinteger(__ls, _st->num_length);
	lua_pushinteger(__ls, _st->elems);
	return 0;
}

int new_W_Elem_Segment(lua_State* __ls) {
	lua_checkstack(__ls, 4);
	varuint32 index = luaL_optinteger(__ls,-4,0);
	init_expr_t* offset = lua_touserdata(__ls,-3);
	varuint32 num_length = luaL_optinteger(__ls,-2,0);
	varuint32* elems = luaL_optinteger(__ls,-1,0);
	W_Elem_Segment* dummy = push_W_Elem_Segment(__ls);
	dummy->index = index;
	dummy->offset = offset;
	dummy->num_length = num_length;
	dummy->elems = elems;
	return 1;
}

static int getter_W_Elem_Segment_index(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	lua_pop(__ls, -1);
	lua_pushinteger(__ls, dummy->index);
	return 1;
}
static int getter_W_Elem_Segment_offset(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	lua_pop(__ls, -1);
	lua_pushlightuserdata(__ls, dummy->offset);
	return 1;
}
static int getter_W_Elem_Segment_num_length(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	lua_pop(__ls, -1);
	lua_pushinteger(__ls, dummy->num_length);
	return 1;
}
static int getter_W_Elem_Segment_elems(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	lua_pop(__ls, -1);
	lua_pushinteger(__ls, dummy->elems);
	return 1;
}

static int setter_W_Elem_Segment_index(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	dummy->index = luaL_checkinteger(__ls, 2);
	lua_settop(__ls, 1);
	return 1;
}
static int setter_W_Elem_Segment_offset(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	dummy->offset = luaL_checkudata(__ls, 2, "W_Elem_Segment");
	lua_settop(__ls, 1);
	return 1;
}
static int setter_W_Elem_Segment_num_length(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	dummy->num_length = luaL_checkinteger(__ls, 2);
	lua_settop(__ls, 1);
	return 1;
}
static int setter_W_Elem_Segment_elems(lua_State* __ls) {
	W_Elem_Segment* dummy = check_W_Elem_Segment(__ls, 1);
	dummy->elems = luaL_checkinteger(__ls, 2);
	lua_settop(__ls, 1);
	return 1;
}

static const luaL_Reg W_Elem_Segment_methods[] = {
	{"new", new_W_Elem_Segment},
	{"set_index", setter_W_Elem_Segment_index},
	{"set_offset", setter_W_Elem_Segment_offset},
	{"set_num_length", setter_W_Elem_Segment_num_length},
	{"set_elems", setter_W_Elem_Segment_elems},
	{"index", getter_W_Elem_Segment_index},
	{"offset", getter_W_Elem_Segment_offset},
	{"num_length", getter_W_Elem_Segment_num_length},
	{"elems", getter_W_Elem_Segment_elems},
	{0,0}
};

static const luaL_Reg W_Elem_Segment_meta[] = {
	{0, 0}
};

int W_Elem_Segment_register(lua_State* __ls) {
	luaL_openlib(__ls, "W_Elem_Segment", W_Elem_Segment_methods, 0);
	luaL_newmetatable(__ls, "W_Elem_Segment");
	luaL_openlib(__ls, 0, W_Elem_Segment_meta, 0);
	lua_pushliteral(__ls, "__index");
	lua_pushvalue(__ls, -3);
	lua_rawset(__ls, -3);
	lua_pushliteral(__ls, "__metatable");
	lua_pushvalue(__ls, -3);
	lua_rawset(__ls, -3);
	lua_pop(__ls, 1);
return 1;
}


