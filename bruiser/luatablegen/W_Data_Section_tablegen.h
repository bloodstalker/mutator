
// automatically generated by luatablegen
#include "../lua-5.3.4/src/lua.h"
#include "../lua-5.3.4/src/lauxlib.h"
#include "../lua-5.3.4/src/lualib.h"
#include <inttypes.h>
#include <stdbool.h>

#ifndef _W_Data_Section_H
#define _W_Data_Section_H
#ifdef __cplusplus
extern "C" {
#endif

#include "../wasm.h"

static W_Data_Section* convert_W_Data_Section (lua_State* __ls, int index);
static W_Data_Section* check_W_Data_Section(lua_State* __ls, int index);
W_Data_Section* push_W_Data_Section(lua_State* __ls);
int W_Data_Section_push_args(lua_State* __ls, W_Data_Section* _st);
int new_W_Data_Section(lua_State* __ls);
static int getter_W_Data_Section_count(lua_State* __ls);
static int getter_W_Data_Section_entries(lua_State* __ls);
static int setter_W_Data_Section_count(lua_State* __ls);
static int setter_W_Data_Section_entries(lua_State* __ls);
int W_Data_Section_register(lua_State* __ls);

#ifdef __cplusplus
}
#endif //end of extern c
#endif //end of inclusion guard


